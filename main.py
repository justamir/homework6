def sum_strings(a, b, c, d):
    result = a + b + c + d
    return result

result = sum_strings('1', '2', '3', '4')
print(result)

myarr = [1, 2, 3, 4, 'a', 'b']
print(myarr, sep = '+++')

my_str = ' hello {}'.format('world')
print(my_str)

my_string = 'book author {author}, book company {company}'.format(author = 'Pushkin', company ='Pushka')
print(my_string)

def create_docs(first_name, last_name, inn, age, gender):
    result = {}

    result['first_name'] = first_name
    result['last_name'] = last_name
    result['inn'] = inn
    result['age'] = age
    result['gender'] = gender

    return result

#first_document = create_docs('Ainazik', 'Benazarova', '16', 'female', 'blonde') -------------ne nado tak delat'

#print(first_document)

first_document = create_docs(
    first_name='Ainazik',
    last_name='Benazarova',
    age='16',
    gender='female',
    inn='2121212489')

print(first_document)


def create_document(*args, **kwargs):
    print('---args----', args)
    print('---kwargs---', kwargs)

create_document(1, 2, 3, 4)
create_document(a=1, b=2, c=3, d=4)
create_document(1, 2, 3, 4, a=1, b=2, c=3, d=4)
create_document(a=12, b=23, c=25, d=115)
create_document()


def create_doc2(first_name, last_name, middle_name, *args, **kwargs):
    result = {
        'first name': first_name,
        'last_name': last_name,
        'middle_name': middle_name,
        'inn': kwargs['inn']
    }
    return result

doki_alibeka = create_doc2('alibek', 'kadyrov', 'petrovich', inn=2212123212)
print(doki_alibeka)


def sum_five(num):
    result = num + 5
    return result
print(result)

sum_five_2 = lambda num: num + 5
print(sum_five_2)


sum_five_3 = lambda num: num / 5
result = sum_five_3(30)
print(result)



result_1 = sum_five_2(25)
result_2 = sum_five(22)
print(result_2)
print(result_1)



my_array = [
    {
        'name': 'Aizhana',
        'age': 26
    },
    {
        'name': 'Zhibeka',
        'age': 17,
    },
    {
        'name': 'Ariana',
        'age': 20
    }
]

my_array.sort(key=lambda girls: girls['age'])
print(my_array)


my_list = [1, 2, 3, 4, 5]              #can be changed
my_list.pop(2)
my_list.append(25)
my_list = set(my_list)
print(my_list)


my_tuple = (1, 2, 3, 4, 5)     #cant be changed like and *args
my_tuple = list(my_tuple)

my_set = {'amir', 'mirlan', 'sanzhar', 'amir', 'mirlan', 'sanzhar'}  #doesn't repeat same things(no dublicates)
#my_set = list(my_set)

my_dict = {}


print(my_set)

my_array = [
    {
        'name': 'Aizhana',
        'age': 26
    },
    {
        'name': 'Zhibeka',
        'age': 17,
    },
    {
        'name': 'Ariana',
        'age': 20
    }
]

print(my_array)
